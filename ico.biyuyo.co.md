# Biyuyo BYY Initial Coin Offering

### How to buy Buyuyo Token (BYY)

1. Create an omni wallet using ​[omniwallet.org](https://www.omniwallet.org)​, [​omni core​](https://www.omnilayer.org/), or ​[holytransaction​](https://holytransaction.com/).
2. Deposit [bitcoin](https://bitcoin.org) (BTC) into your omni wallet.
3. Send bitcoin (BTC) to the Biyuyo Contract Address:
    [1EuukFjK3Hte2YkEvS4nZXassGTHgb7Pyw](https://www.omniexplorer.info/address/1EuukFjK3Hte2YkEvS4nZXassGTHgb7Pyw)

After 10 confirmations, Biyuyo S.A. will issue you 1 BYY for each 1.01 BTC sent, back to your sending address from #3.

### Biyuyo Token Uses

● Hold on to it for 0.01 BTC reward / mo / BYY
● Use it on the Biyuyo transaction accelerator (coming soon)
● Subscribe to Biyuyo S.A. financial updates
● Send it to your friends using omni wallets
● Trade it on the ​omni-dex

### What happens to Biyuyo tokens at the end of their life?

Biyuyo S.A. receives them for services or buys them back on the open market and destroys
them.
